﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Automata
{
    public class TimbuktuSolver : BDDAlgebra
    {
        /// <summary>
        /// Returns BDD from string representation
        /// </summary>
        /// <param name="bin"> string representation of BDD, example: 10X0 is a BDD with depth 4 where X means that both 1 and 0 should go to true </param>
        /// <returns></returns>
        public BDD GetBDDFromBinary(string bin)
        {
            //bool lastXs = true;
            BDD last = True;

            for (int i = bin.Length - 1, ordinal = 0; i >= 0; --i, ++ordinal)
            {
                //if (lastXs && bin[i] == 'X')
                if (bin[i] == 'X')
                {
                    continue;
                }
                /*else
                {
                    lastXs = false;
                }
                if (bin[i] == 'X')
                {
                    last = MkBvSet(ordinal, last, last);
                }*/
                else if (bin[i] == '1')
                {
                    last = MkBvSet(ordinal, last, False);
                }
                else if (bin[i] == '0')
                {
                    last = MkBvSet(ordinal, False, last);
                }
                else
                {
                    throw new AutomataException("Wrong binary represantation of BDD!");
                }

            }

            return last;
        }
    }
}
