﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Automata;
using Microsoft.Automata.BooleanAlgebras;
using System.IO;

namespace SimulationExperiments
{
    /// <summary>
    /// Class for running simulation experiments on SFA
    /// </summary>
    /// <typeparam name="S"> the type of the labels on moves in automaton </typeparam>
    public class SimulationRunExperiment<S>
    {
        /// <summary>
        /// which algorithm to use for simulation
        /// </summary>
        private int which = 0;
        /// <summary>
        /// how many times should the algorithm be run on the sfa (if zero, then we compare the runtimes of direct determinstic minimization with det. min. after simulation)
        /// </summary>
        private int howMuch = 0;
        /// <summary>
        /// sfa on which we will run experiments
        /// </summary>
        private Automaton<S> sfa = null;
        /// <summary>
        /// original sfa, for direct det. min.
        /// </summary>
        private Automaton<S> orig = null;
        /// <summary>
        /// Saves and prepares SFA for experiments
        /// </summary>
        public Automaton<S> SFA
        {
            set
            {
                this.sfa = value.RemoveEpsilons().EliminateDeadStatesBetter()./*MakeOnlyOneStateFinal().*/RemoveEpsilonLoops().RemapStates();
                lastFinalStates = new HashSet<int> { sfa.InitialState };
                this.orig = value.RemoveEpsilons().EliminateDeadStatesBetter().RemoveEpsilonLoops().RemapStates();
            }
        }

        /// <summary>
        /// final states before sfa was reversed
        /// </summary>
        IEnumerable<int> lastFinalStates;
        /// <summary>
        /// reverses SFA
        /// </summary>
        public Automaton<S> REVSFA
        {
            set
            {
                // sfa = sfa.Reverse();
                List<Move<S>> reversed_moves = new List<Move<S>>();
                foreach (var move in value.GetMoves())
                    reversed_moves.Add(Move<S>.Create(move.TargetState, move.SourceState, move.Label));
                sfa = Automaton<S>.Create(value.Algebra, value.InitialState, lastFinalStates, reversed_moves);
                lastFinalStates = value.GetFinalStates();
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="which"> which algorithm to use for simulation </param>
        /// <param name="howMuch"> how many times should the algorithm be run on the sfa (if zero, then we compare the runtimes of direct determinstic minimization with det. min. after simulation) </param>
        public SimulationRunExperiment(int which = 0, int howMuch = 0)
        {
            this.which = which;
            this.howMuch = howMuch;
        }
        
        /// <summary>
        /// returns automaton after simulation-based reduction
        /// </summary>
        public Automaton<S> getMinAutomaton()
        {
            return sfa.MakeTotal().NonDetStateRedBySim(ref lastFinalStates, which).EliminateDeadStatesBetter();
        }

        /// <summary>
        /// Run experiments (either runs simulation howMuch times with chosen algorithm and saves median, or compares det. min. with sim.)
        /// </summary>
        public string Run()
        {
            Stopwatch watch = new Stopwatch();
            string str = sfa.StateCount + "," + sfa.MoveCount + ",";
            
            if (howMuch == 0)
            {
                /*
                var whichOld = which;
                which = 99;
                // bisimulation (should be tested that we want that)
                try
                {
                    int movecount;
                    bool thisIsReversed = false;
                    watch.Restart();
                    do
                    {
                        movecount = sfa.MoveCount;
                        REVSFA = getMinAutomaton();
                        thisIsReversed = !thisIsReversed;

                    } while (movecount != sfa.MoveCount);
                    watch.Stop();
                    var bisimTime = watch.Elapsed.TotalMilliseconds;

                    if (thisIsReversed)
                        REVSFA = sfa;

                    str += sfa.StateCount + "," + sfa.MoveCount + "," + bisimTime + ",";

                    // direct (or after bisimulation) determinization and minimization
                    try
                    {
                        // determinize and get min
                        watch.Start();
                        var detMin = sfa.Determinize(100000).Minimize().EliminateDeadStatesBetter();
                        watch.Stop();
                        str += detMin.StateCount + "," + detMin.MoveCount + "," + watch.Elapsed.TotalMilliseconds;
                    }
                    catch
                    {
                        str += "-1,-1,-1";
                    }

                }
                catch
                {
                    str += "-1,-1,-1,-1,-1,-1";
                }
                

                which = whichOld;
                sfa = orig;
                lastFinalStates = new HashSet<int> { sfa.InitialState };

                str += ",";
                */

                // reduce automaton by reducing original, then reverse, then reverse,.... until the number of transitions stays the same
                try
                {
                    int movecount;
                    bool thisIsReversed = false;
                    watch.Restart();
                    do
                    {
                        movecount = sfa.MoveCount;
                        REVSFA = getMinAutomaton();
                        thisIsReversed = !thisIsReversed;
                    } while (movecount != sfa.MoveCount);
                    watch.Stop();
                    var simTime = watch.Elapsed.TotalMilliseconds;

                    if (thisIsReversed)
                        REVSFA = sfa;

                    str += sfa.StateCount + "," + sfa.MoveCount + "," + simTime + ",";
                }
                catch
                {
                    return str + "-1,-1,-1,-1,-1,-1";
                }
                
                // determinization and minimization after (bi)simulation
                try
                {
                    // determinize and get min
                    watch.Start();
                    var detMin = sfa.Determinize(100000).Minimize().EliminateDeadStatesBetter();
                    watch.Stop();
                    str += detMin.StateCount + "," + detMin.MoveCount + "," + watch.Elapsed.TotalMilliseconds;
                }
                catch
                {
                    str += "-1,-1,-1";
                }

                return str;
            }

            /*// run algorithm so it does not interfere with the time (because first run is always slower)
            try
            {
                var minSim = getMinAutomaton();
                str += minSim.StateCount;
            }
            catch (Exception e)
            {
                return str + "-1,-1,0";
            }*/

            //Automaton<S> minSim = null; 
            bool[,] sim = null;

            // run algorithm howMuch times and get median
            List<double> time = new List<double>();
            
            for (int i = 0; i < howMuch; ++i)
            {
                try
                {
                    Automaton<S>.numOfMinterms = 0;
                    Automaton<S>.debug_numOfMoves = 0;
                    watch.Restart();
                    //minSim = getMinAutomaton();
                    if (which == 4)
                    {
                        sim = sfa.getSimulation(which);
                    }
                    else
                    {
                        sim = sfa.MakeTotal().getSimulation(which);
                    }

                   // sim = sfa.MakeTotal().getSimulation(which);

                    watch.Stop();
                    time.Add(watch.Elapsed.TotalMilliseconds);
                }
                catch (Exception e)
                {
                    time.Add(-1);
                    break;
                }
            }

            //if (minSim != null)
            if (sim != null)
            {
                //str += minSim.StateCount;
                int j = 0;
                foreach (var b in sim)
                {
                    if (b)
                        ++j;
                }
                str += j;
            } else
            {
                str += -1;
            }

            time.Sort();
            return str + "," + time[time.Count / 2] + "," + time.Count((x) => x != -1) + "," + Automaton<S>.numOfMinterms + "," + Automaton<S>.debug_numOfMoves;
        }

        
    }


    /// <summary>
    /// Static methods for running experiments
    /// </summary>
    public class SimulationNFAExperiment
    {
        /// <summary>
        /// prefix of the name of file to which results of experiments are saved
        /// </summary>
        string file = "";
        /// <summary>
        /// true if we want to compare dir. min. with sim.
        /// </summary>
        private bool statesComp = false;

        private int which; // which algorithm
        private int howMuch; // how much to run it (we get average)
        
        //SimulationRunExperiment<S> exper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="file"> prefix of the name of file to which results of experiments are saved </param>
        /// <param name="which"> which algorithm to use for simulation </param>
        /// <param name="howMuch"> how many times should the algorithm be run on the sfa (if zero, then we compare the runtimes of direct determinstic minimization with det. min. after simulation) </param>
        public SimulationNFAExperiment(string file = "", int which = 0, int howMuch = 0)
        {
            this.file = file;

            //exper = new SimulationRunExperiment<S>(which, howMuch);
            this.which = which;
            this.howMuch = howMuch;

            if (howMuch == 0)
            {
                statesComp = true;
            }
        }
        
        // header of files
        // this is for comparing dir. min. and sim.
        static string headerStateCount = "id,states,transitions" + /*",statesBisim,transitionsBisim,timeBisim,stateMinAfterBisim,transitionsMinAfterBisim,timeBisimAndMin" + */ ",statesSim,transitionsSim,timeSim,stateMinAfterSim,transitionsMinAfterSim,timeSimAndMin";
        // this is only for getting the runtimes of algorithms
        static string headerTime = "id,states,transitions,simRelSize,time,goodruns,minterms,mintermizedAutomatonTransitions";

        /// <summary>
        /// Prints header
        /// </summary>
        /// <param name="file"> file to which the header is printed </param>
        public void PrintHeader(string file)
        {
            var header = statesComp ? headerStateCount : headerTime;
            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(Program.pathOutput + file + ".csv", false))
            {
                outfile.WriteLine(header);
            }

        }

        /// <summary>
        /// Print headers to two files
        /// </summary>
        /// <param name="file"> file to which the header is printed </param>
        public void PrintHeaderAlsoReverse(string file)
        {
            PrintHeader(file + "Normal");
            //PrintHeader(file + "Reverse");
        }

        /// <summary>
        /// Run experiments
        /// </summary>
        /// <param name="sfa"> SFA on which we want to run experiments </param>
        /// <param name="id"> ID of experiment </param>
        /// <param name="outputFileName"> name of file to which results will be appeded </param>
        public void Run<S>(Automaton<S> sfa, string id, string outputFileName = "")
        {
            var exper = new SimulationRunExperiment<S>(which, howMuch);
            exper.SFA = sfa;
            string line = id + "," + exper.Run();
            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(Program.pathOutput + outputFileName + ".csv", true))
            {
                outfile.WriteLine(line);
            }
        }

        /// <summary>
        /// Run experiments also on reverse SFA
        /// </summary>
        /// <param name="sfa"> SFA on which we want to run experiments </param>
        /// <param name="id"> ID of experiment </param>
        /// <param name="outputFileName"> name of file to which results will be appeded </param>
        public void RunAlsoReverse<S>(Automaton<S> sfa, string id, string outputFileName = "")
        {
            Run(sfa, id, outputFileName + "Normal");
            //Run(sfa.Reverse(), id, outputFileName + "Reverse");
        }

        /// <summary>
        /// Run experiments for regular expressions
        /// </summary>
        public void RunRegexExperiments(int start = 0, int end = 10000)
        {
            if (statesComp)
                PrintHeader(file + "Regex");
            else
                PrintHeaderAlsoReverse(file + "Regex");

            var lines = File.ReadAllLines(Program.path + Program.regexInputFile);

            var rex = new Microsoft.Automata.Rex.RexEngine(BitWidth.BV16);
            var solver = rex.Solver;

            Console.WriteLine("Running experiments on regular expressions.");
            for (int i = start; i < Math.Min(lines.Length, end); i++)
            {
                string regex = lines[i].Trim();
                Console.WriteLine("Experiment " + (i+1) + "/" + lines.Length);
                var sfa = rex.CreateFromRegexes(regex);
                if (statesComp)
                    Run(sfa, i.ToString(), file + "Regex");
                else
                    RunAlsoReverse(sfa, i.ToString(), file + "Regex");
            }
        }

        /// <summary>
        /// Creates edge case automaton that shows when global mint. algorithm fails
        /// </summary>
        /// <param name="depth"> depth of the automaton, the number of states will be 2*depth+1</param>
        /// <returns></returns>
        public static Automaton<BDD> CreateEdgeCaseAutomaton(int depth = 5)
        {
            var solver = new BDDAlgebra();
            List<Move<BDD>> moves = new List<Move<BDD>>();
            if (depth > 1)
            {
                moves.Add(new Move<BDD>(0, 1, solver.MkBitTrue(0)));
                moves.Add(new Move<BDD>(0, 2, solver.MkBitTrue(0)));
            }

            var iLast = solver.MkBitTrue(0);
            for (int i = 1; i < depth - 1; ++i)
            {
                var iNow = solver.MkBitTrue(i);
                moves.Add(new Move<BDD>(i, i + 1, iNow));
                moves.Add(new Move<BDD>(i, i, iLast));
                moves.Add(new Move<BDD>(i + depth - 1, i + depth, iNow));

            }
            
            Automaton<BDD> sfa = Automaton<BDD>.Create(solver, 0, depth == 1 ? new List<int>() : new List<int> { depth - 1, (depth - 1) * 2 }, moves);
            return sfa;
        }

        /// <summary>
        /// Run experiments on edge case SFAs
        /// </summary>
        /// <param name="max"> maximal depth of edge case SFA, starts with 2 </param>
        public void RunEdgeCase(int max = 10)
        {
            PrintHeader(file + "Edgecase");

            Console.WriteLine("Running experiments for edge case automata.");
            for (int i = 2; i < max; ++i)
            {
                var sfa = CreateEdgeCaseAutomaton(i);
                Console.WriteLine("Depth " + i + "/" + max);
                Run(sfa, i.ToString(), file + "Edgecase");
            }
        }

        /// <summary>
        /// Run experiments on SFAs created during WS1S decision procedure
        /// </summary>
        public void RunTimbutktuExperiments()
        {
            if (statesComp)
                PrintHeader(file + "WS1S");
            else
                PrintHeaderAlsoReverse(file + "WS1S");

            Console.WriteLine("Running experiments on automata from WS1S.");
            var folderName = Program.timbukFolder + (statesComp ? @"nondet\" : "");
            foreach (var fileName in Directory.EnumerateFiles(folderName, "*.*", SearchOption.AllDirectories))
            {
                var timFile = Path.GetFileName(fileName);
                //try
                //{
                

                    Console.WriteLine("Processing automaton in file " + timFile);
        
                    var sfa = TimbukNFAParserDif.ParseVataFile(fileName);

                    Console.WriteLine("Processed, running experiments");

                    if (statesComp)
                        Run(sfa, timFile, file + "WS1S");
                    else
                        RunAlsoReverse(sfa, timFile, file + "WS1S");
                //}
                //catch (Exception e)
                //{
                //    Console.WriteLine("SFA from " + timFile + " was problematic.");
                //    Console.WriteLine(e);
                //}
            }
        }

        public void RunGoodForBlockExp()
        {
            if (statesComp)
                PrintHeader(file + "BlockEdge");
            else
                PrintHeaderAlsoReverse(file + "BlockEdge");

            var alg = new FiniteSetAlgebra<string>(new HashSet<string> { "a", "b", "c" });

            List<Move<UIntW>> moves = new List<Move<UIntW>>();
            int n = 5000;
            for (int i = 1; i < 2 * n; i++)
            {
                moves.Add(new Move<UIntW>(0, i, alg.MkAtom("c")));
                if (i < n)
                    moves.Add(new Move<UIntW>(i, 2 * n, alg.MkAtom("a")));
                else
                    moves.Add(new Move<UIntW>(i, 2 * n, alg.MkAtom("b")));
            }
            moves.Add(new Move<UIntW>(2 * n, 2 * n, alg.True));

            var sfa = Automaton<UIntW>.Create(alg, 0, new HashSet<int> { 2 * n }, moves);
            if (statesComp)
                Run(sfa, "one", file + "BlockEdge");
            else
                RunAlsoReverse(sfa, "one", file + "BlockEdge");
            return;
        }

        public void RunClassicalNFAExperiments()
        {
            if (statesComp)
                PrintHeader(file + "Classic");
            else
                PrintHeaderAlsoReverse(file + "Classic");
            
            Console.WriteLine("Running experiments on classical automata.");
            var folderName = Program.NFAFolder;
            foreach (var fileName in Directory.EnumerateFiles(folderName, "*.*", SearchOption.AllDirectories))
            {
                var timFile = Path.GetFileName(fileName);
                try
                {
                Console.WriteLine("Processing automaton in file " + timFile);
                var sfa = TimbukNFAParserDif.ParseVataFileFinSet(fileName);

                Console.WriteLine("Processed, running experiments");

                if (statesComp)
                    Run(sfa, fileName, file + "Classic");
                else
                    RunAlsoReverse(sfa, fileName, file + "Classic");
                }
                catch (Exception e)
                {
                    Console.WriteLine("SFA from " + fileName + " was problematic.");
                    Console.WriteLine(e);
                }
            }
        }
    }
}
