﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

using Microsoft.Automata;
using System.Diagnostics;

namespace SimulationExperiments
{
    class Program
    {
        // benchmarks directory
        static internal string path = @"..\..\benchmark\";
        // output directory
        static internal string pathOutput = @"..\..\results\";

        static internal string regexInputFile = "regexes.txt";

        static internal string timbukFolder = path + @"timbuktu\";

        static internal string NFAFolder = path + @"NFA\IBakery5PUnrEnc-FbtOneOne-Nondet";

        static internal string timbukOutput = pathOutput + @"globalMinterm\";

        // number of experiments for algorithms (0 means compare with bisim)
        static internal int numberOfExperiments = 1;

        static internal Dictionary<int, string> algName = new Dictionary<int, string> { { 0, "localSFAnoopt" }, { 1, "localSFA" }, { 2, "nocountSFAnoopt" }, { 3, "nocountSFA" }, { 4, "globalSFA" }, { 5, "blockSFA" }, { 6, "nocountSFAuseconj" }, { 7, "globalSFAcomplete" }, { 99, "bisim" } };

        static void Main(string[] args)
        {
            SimulationNFAExperiment experiment = null;

            // return
            
            
            /* var lines = File.ReadAllLines(Program.path + Program.regexInputFile);

             var rex = new Microsoft.Automata.Rex.RexEngine(BitWidth.BV16);
             var solver = rex.Solver;

             Console.WriteLine("Comparing simulation of SFAs from regexes:");
             for (int i = 0; i < Math.Min(lines.Length, 100000); i++)
             {
                 string regex = lines[i].Trim();
                 Console.WriteLine("SFA " + (i + 1) + "/" + lines.Length);
                 var sfa = rex.CreateFromRegexes(regex).RemoveEpsilons().EliminateDeadStatesBetter().RemoveEpsilonLoops().RemapStates();
                 var simm = sfa.getSimulation(4);
                 var sim = sfa.MakeTotal().getSimulation(3);
                 for (int j = 0; j < simm.GetLength(0); ++j)
                 {
                     for (int k = 0; k < simm.GetLength(1); ++k)
                     {
                         if (sim[j, k] != simm[j, k])
                             return;
                     }
                 }
             }


             return;*/



            //cmpSimTimbuk();



            //cmpSimRegex();




            //return;
            HashSet<int> whichAlgs = new HashSet<int> {3 };

            foreach (int i in whichAlgs)
            {
                for (int j = 0; j < 1; ++j)
                {
                    Console.WriteLine("Experiments for algorithm " + algName[i]);
                    experiment = new SimulationNFAExperiment(algName[i] + j, i, numberOfExperiments);
                    // output in <name>RegexNormal.csv
                    //experiment.RunRegexExperiments();
                    // output in <name>TimbuktNormal.csv
                    //experiment.RunTimbutktuExperiments();
                    // output in <name>Edgecase.csv
                    //experiment.RunEdgeCase(20);
                    // output in <name>Classic
                    experiment.RunClassicalNFAExperiments();
                }
            }
            /*
            for (int i = 0; i < 10; ++i)
            {
                WS1SGlobalToTimbuk(i.ToString());
                RegexAutomataToGlobalMintermFile(i.ToString());
            }*/

            return;
            
        }

        static public void WS1SGlobalToTimbuk(string file = "")
        {
            var dir = Program.timbukOutput + @"ws1s\";

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(dir + "mintermization" + file + ".csv", false))
            {
                outfile.WriteLine("id,time");
            }

            Console.WriteLine("Running experiments on automata from WS1S.");
            var folderName = Program.timbukFolder;
            foreach (var fileName in Directory.EnumerateFiles(folderName, "*.*", SearchOption.AllDirectories))
            {
                var timFile = Path.GetFileName(fileName);
                Console.WriteLine("Processing automaton in file " + timFile);
                var sfa = TimbukNFAParserDif.ParseVataFile(fileName).RemoveEpsilons().EliminateDeadStatesBetter().RemoveEpsilonLoops().RemapStates().MakeTotal();
                Stopwatch watch = new Stopwatch();
                try
                {
                    watch.Restart();
                    var timbuk = sfa.GlobalMintermToTimbuk("A");
                    watch.Stop();

                    System.IO.File.WriteAllLines(dir + timFile + ".timbuk", timbuk);
                }
                catch
                {
                    using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(dir + "mintermization" + file + ".csv", true))
                    {
                        outfile.WriteLine(timFile + ",-1");
                    }
                    continue;
                }


                using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(dir + "mintermization" + file + ".csv", true))
                {
                    outfile.WriteLine(timFile + "," + watch.Elapsed.TotalMilliseconds);
                }
            }
        }

        /// <summary>
        /// Save global minterm SFAs from regular expressions
        /// </summary>
        static public void RegexAutomataToGlobalMintermFile(string file = "", int start = 0, int end = 10000)
        {
            var lines = File.ReadAllLines(Program.path + Program.regexInputFile);

            var rex = new Microsoft.Automata.Rex.RexEngine(BitWidth.BV16);
            var solver = rex.Solver;

            var dir = Program.timbukOutput + @"regex\";

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(dir + "mintermization" + file + ".csv", false))
            {
                outfile.WriteLine("id,time");
            }

            Console.WriteLine("Saving global minterm SFAs from regexes:");
            for (int i = start; i < Math.Min(lines.Length, end); i++)
            {
                string regex = lines[i].Trim();
                Console.WriteLine("SFA " + (i + 1) + "/" + lines.Length);
                var sfa = rex.CreateFromRegexes(regex).RemoveEpsilons().EliminateDeadStatesBetter().RemoveEpsilonLoops().RemapStates().MakeTotal();
                Stopwatch watch = new Stopwatch();
                watch.Restart();
                var timbuk = sfa.GlobalMintermToTimbuk(i.ToString());
                watch.Stop();

                System.IO.File.WriteAllLines(dir + i.ToString() + ".timbuk", timbuk);

                using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(dir + "mintermization" + file + ".csv", true))
                {
                    outfile.WriteLine(i + "," + watch.Elapsed.TotalMilliseconds);
                }
            }
        }

        static public void cmpSimRegex(int start = 0, int end = 10000)
        {
            var lines = File.ReadAllLines(Program.path + Program.regexInputFile);

            var rex = new Microsoft.Automata.Rex.RexEngine(BitWidth.BV16);
            var solver = rex.Solver;

           /* using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(Program.timbukOutput + "mintermization.csv", false))
            {
                outfile.WriteLine("id,time");
            }*/

            Console.WriteLine("Comparing simulation of SFAs from regexes:");
            for (int i = start; i < Math.Min(lines.Length, end); i++)
            {
                string regex = lines[i].Trim();
                //Console.WriteLine("SFA " + (i + 1) + "/" + lines.Length);
                var sfa = rex.CreateFromRegexes(regex).RemoveEpsilons().EliminateDeadStatesBetter().RemoveEpsilonLoops().RemapStates().MakeTotal();
                var sim = sfa.getSimulation(3);
                Console.WriteLine(cmpSimFromFile(sim, @"..\..\results\simulationforglobal\regex\" + i + ".sim") + " " + i);                    
            }
        }

        static public void cmpSimTimbuk()
        {
            Console.WriteLine("Comparing simulation of SFAs from WS1S.");
            var folderName = Program.timbukFolder;
            foreach (var fileName in Directory.EnumerateFiles(folderName, "*.*", SearchOption.AllDirectories))
            {
                var timFile = Path.GetFileName(fileName);
                //Console.WriteLine("Processing automaton in file " + timFile);
                if (!File.Exists(@"..\..\results\simulationforglobal\ws1s\" + timFile + ".sim"))
                {
                    Console.WriteLine("does not exists: " + timFile);
                    continue;
                }

                var sfa = TimbukNFAParserDif.ParseVataFile(fileName).RemoveEpsilons().EliminateDeadStatesBetter().RemoveEpsilonLoops().RemapStates().MakeTotal();

                var sim = sfa.getSimulation(3);
                Console.WriteLine(cmpSimFromFile(sim, @"..\..\results\simulationforglobal\ws1s\" + timFile + ".sim") + ": " + timFile);
                
            }
        }

        static public bool cmpSimFromFile(bool[,] sim, string pathFN )
        {
            var lines = File.ReadAllLines(pathFN);

            Dictionary<int, int> mapping = new Dictionary<int, int>();
            var mappingString = lines[0].Substring(0, lines[0].Length - 1).Split(',');
            foreach (var map in mappingString)
            {
                if (map == "")
                    continue;

                var mapDiv = map.Split(':');
                int mapFrom = Int32.Parse(mapDiv[0]);
                int mapTo = Int32.Parse(mapDiv[1].Substring(2));
                mapping[mapFrom] = mapTo;
            }
            int numOfStates = mapping.Count;

            bool[,] simFile = new bool[numOfStates, numOfStates];
            var simPairs = lines[1].Substring(2, lines[1].Length - 4).Split(new string[] { "), (" }, StringSplitOptions.None);
            foreach(var pair in simPairs)
            {
                var pairDiv = pair.Split(',');
                int down = Int32.Parse(pairDiv[0]);
                int up = Int32.Parse(pairDiv[1]);
                simFile[mapping[down], mapping[up]] = true;
            }
            

            for (int i = 0; i < simFile.GetLength(0); ++i)
            {
                for (int j = 0; j < simFile.GetLength(1); ++j)
                {
                    if (sim.GetLength(0) != simFile.GetLength(0))
                        return false;
                    if (sim[i, j] != simFile[i, j])
                        return false;
                }
            }
            return true;
        }
    }
}
